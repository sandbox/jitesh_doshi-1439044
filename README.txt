This Drupal module enhances webforms such that a select list (or a dropdown,
group of radio buttons or checkboxes) starts losing options as those options
get used in webform submissions. This is very useful for situations where an
option must be used only once in each webform submission, and when it must
disappear from the available list of choices once it has been used.

A real-world scenario is when the webform is used to reserve seats in a
theatre. Let's say you have 10 seats - A1, A2, A3, A4, A5, B1, B2, B3, B4, B5.
Say the above are show either as a listbox or as a group of checkboxes.
Initially, the webform will show all 10 of them as available options. Once
the visitors of your Drupal website start making webform submissions, the
options (or checkboxes) that are used in those submissions start disappearing
from the subsequent displays of that webform.

HOW TO USE THIS MODULE
----------------------
- Download, install and enable this module.
- Create a webform with a 'select' component.
- In the webform component edit form, look for the checkbox named 'limited
  supply' and enable/select/check it.

That's it. From now on, the component will start behaving in the manner described above.
