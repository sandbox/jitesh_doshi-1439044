(function ($) {
  Drupal.behaviors.webform_single_use_options = {
    attach: function(context, settings) {
      $("select option").each(function() {
        var $thisOption = $(this);
        $.each(Drupal.settings.webform_single_use_options.array_value, function(index, value) {
          if($thisOption.val() === value) {
            $thisOption.attr("disabled", "disabled");
          }
        });
      });
    }
  };
})(jQuery);
